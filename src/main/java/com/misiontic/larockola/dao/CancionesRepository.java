/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.dao;

import com.misiontic.larockola.Models.Canciones;
import com.misiontic.larockola.Models.Generos;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leonardo
 */
@Repository
public interface CancionesRepository extends JpaRepository<Canciones, Integer> {
    Canciones findByNombre(String nombre);
    List<Canciones> findByAlbum(String album);
    List<Canciones> findByInterprete(String interprete);
    List<Canciones> findByIdGenero(Generos genero);
}
