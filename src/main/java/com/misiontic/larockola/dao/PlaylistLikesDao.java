/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.dao;

import com.misiontic.larockola.Models.PlaylistLikes;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Leonardo
 */
public interface PlaylistLikesDao extends CrudRepository<PlaylistLikes,Integer> {
    @Query(value = "select count(*) from playlistlikes p where p.idplaylist = :id", nativeQuery = true)
    Integer countLikesByIdPlayist(@Param("id") int id);    
}
