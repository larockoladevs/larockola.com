/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.dao;

import com.misiontic.larockola.Models.Playlists;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Leonardo
 */
public interface PlaylistsDao extends CrudRepository<Playlists,Integer> {
    @Query(value = "select * from playlists p where p.indFavoritas = 'S' and p.idUsuario = :id", nativeQuery = true)
    Playlists findFavByIdusuario(@Param("id") int id);
}
