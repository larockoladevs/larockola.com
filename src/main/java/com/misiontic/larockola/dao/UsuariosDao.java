/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.dao;

import com.misiontic.larockola.Models.Usuarios;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Leonardo
 */
public interface UsuariosDao extends CrudRepository<Usuarios,Integer> {

}