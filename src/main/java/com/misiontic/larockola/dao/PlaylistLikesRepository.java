/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.dao;

import com.misiontic.larockola.Models.PlaylistLikes;
import com.misiontic.larockola.Models.Playlists;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Leonardo
 */
@Repository
public interface PlaylistLikesRepository  extends JpaRepository<PlaylistLikes, Integer> {
    List<PlaylistLikes> findByIdPlaylist(Playlists IdPlaylist);
}
