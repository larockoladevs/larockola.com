/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Controllers;

import com.misiontic.larockola.Models.Canciones; 
import com.misiontic.larockola.Models.Generos;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus; 
import org.springframework.http.ResponseEntity; 
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController; 
import com.misiontic.larockola.Services.CancionesService;

/**
 *
 * @author Leonardo
 */
@RestController 
@CrossOrigin("*") 
@RequestMapping("/canciones")
public class CancionesController {
    
    @Autowired 
    private CancionesService cancionservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Canciones> agregar(@RequestBody Canciones cancion){
        Canciones obj = cancionservice.save(cancion);
        return new ResponseEntity<>(obj, HttpStatus.OK);        
    }
    
    @DeleteMapping(value="delete/{id}")     
    public ResponseEntity<Canciones> eliminar(@PathVariable Integer id){ 
        Canciones obj = cancionservice.findById(id);
        if(obj!=null)     
            cancionservice.deleteById(id);
        else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }  
    
    @PutMapping(value="/") 
    public ResponseEntity<Canciones> editar(@RequestBody Canciones cancion){ 
        Canciones obj = cancionservice.findById(cancion.getIdCancion()); 
        if(obj!=null) {     
            obj.setNombre(cancion.getNombre());
            obj.setInterprete(cancion.getInterprete());
            obj.setAnio(cancion.getAnio());
            obj.setCompositor(cancion.getCompositor());
            obj.setLetras(cancion.getLetras());
            obj.setLinkStreaming(cancion.getLinkStreaming());
            obj.setAlbum(cancion.getAlbum());
            cancionservice.save(obj);
        } else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list") 
    public List<Canciones> consultarTodo(){ 
        return cancionservice.findAll(); 
    }  
    
    @GetMapping("/list/{id}") 
    public Canciones consultaPorId(@PathVariable Integer id){ 
        return cancionservice.findById(id);
    }

    @GetMapping("/list/byname/{nombre}") 
    public Canciones consultaPorNombre(@PathVariable String nombre){ 
        return cancionservice.findByNombre(nombre);
    }
    
    @GetMapping("/list/byalbum/{album}") 
    public List<Canciones> consultaPorAlbum(@PathVariable String album){ 
        return cancionservice.findByAlbum(album);
    }    

    @GetMapping("/list/byinterprete/{interprete}") 
    public List<Canciones> consultaPorInterprete(@PathVariable String interprete){ 
        return cancionservice.findByInterprete(interprete);
    } 

    @GetMapping("/list/bygenero/") 
    public List<Canciones> consultaPorGenero(@RequestBody Generos genero){ 
        return cancionservice.findByIdGenero(genero);
    } 
    
}