/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Controllers;

import com.misiontic.larockola.Models.Usuarios; 
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus; 
import org.springframework.http.ResponseEntity; 
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController; 
import com.misiontic.larockola.Services.UsuariosService;
import org.apache.commons.lang3.RandomStringUtils;
import com.misiontic.larockola.Security.ManejoClaves;

/**
 *
 * @author Leonardo
 */
@RestController 
@CrossOrigin("*") 
@RequestMapping("/usuarios")
public class UsuariosController {
    @Autowired 
    private UsuariosService usuarioservice;
    
    private byte[] saltByte;
    private String saltStr;
    private String pwd;
            
    @PostMapping(value="/")
    public ResponseEntity<Usuarios> agregar(@RequestBody Usuarios usuario){
        Usuarios objExist = usuarioservice.findByEmail(usuario.getEmail());
        if (objExist==null) {
            saltStr = RandomStringUtils.random(10, true, true);
            saltByte = saltStr.getBytes();
            pwd = ManejoClaves.hash(usuario.getPwd(),saltByte);
            usuario.setRandomsalt(saltStr);
            usuario.setPwd(pwd);
            Usuarios objNuev = usuarioservice.save(usuario);
            return new ResponseEntity<>(objNuev, HttpStatus.OK);
        } else
            return new ResponseEntity<>(objExist, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @DeleteMapping(value="delete/{id}")     
    public ResponseEntity<Usuarios> eliminar(@PathVariable Integer id){ 
        Usuarios obj = usuarioservice.findById(id);
        if(obj!=null)     
            usuarioservice.deleteById(id);
        else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }  
    
    @PutMapping(value="/") 
    public ResponseEntity<Usuarios> editar(@RequestBody Usuarios usuario){ 
        Usuarios obj = usuarioservice.findById(usuario.getIdUsuario()); 
        if(obj!=null) {     
            obj.setNombre(usuario.getNombre());
            obj.setRol(usuario.getRol());
            usuarioservice.save(obj);
        } else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }

    @PutMapping(value="cambiarpwd/") 
    public ResponseEntity<Usuarios> cambiarPwd(@RequestBody Usuarios usuario){ 
        Usuarios obj = usuarioservice.findByEmail(usuario.getEmail());
        if(obj!=null) {
            saltStr = RandomStringUtils.random(10, true, true);
            saltByte = saltStr.getBytes();
            pwd = ManejoClaves.hash(usuario.getPwd(),saltByte);            
            obj.setRandomsalt(saltStr);
            obj.setPwd(pwd);
            usuarioservice.save(obj);
        } else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list") 
    public List<Usuarios> consultarTodo(){ 
        return usuarioservice.findAll(); 
    }  
    
    @GetMapping("/list/{id}") 
    public Usuarios consultaPorId(@PathVariable Integer id){ 
        return usuarioservice.findById(id);
    }

    @GetMapping("/list/byemail/{email}") 
    public Usuarios consultaPorEmail(@PathVariable String email){ 
        return usuarioservice.findByEmail(email);
    }     

    @GetMapping("/list/idbyemail/{email}") 
    public Integer consultaIdPorEmail(@PathVariable String email){ 
        Usuarios usuario = usuarioservice.findByEmail(email);
        if (usuario!=null)
            return usuario.getIdUsuario();
        return -1;        
    }

}
