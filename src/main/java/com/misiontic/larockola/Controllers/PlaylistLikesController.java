/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Controllers;

import com.misiontic.larockola.Models.PlaylistLikes;
import com.misiontic.larockola.Models.Playlists;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus; 
import org.springframework.http.ResponseEntity; 
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController; 
import com.misiontic.larockola.Services.PlaylistLikesService;

/**
 *
 * @author Leonardo
 */
@RestController 
@CrossOrigin("*") 
@RequestMapping("/playlistlikes")
public class PlaylistLikesController {
    @Autowired 
    private PlaylistLikesService playlistLikeService;

    @PostMapping(value="/")
    public ResponseEntity<PlaylistLikes> agregar(@RequestBody PlaylistLikes playlistLike){
        PlaylistLikes obj = playlistLikeService.save(playlistLike);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="delete/{id}")     
    public ResponseEntity<PlaylistLikes> eliminar(@PathVariable Integer id){ 
        PlaylistLikes obj = playlistLikeService.findById(id);
        if(obj!=null)     
            playlistLikeService.deleteById(id);
        else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }  
    
    @PutMapping(value="/") 
    public ResponseEntity<PlaylistLikes> editar(@RequestBody PlaylistLikes playlistLike){ 
        PlaylistLikes obj = playlistLikeService.findById(playlistLike.getIdPlaylistlikes());
        if(obj!=null) {     
            obj.setIdPlaylist(playlistLike.getIdPlaylist());
            obj.setIdUsuario(playlistLike.getIdUsuario());
            obj.setFechaLike(playlistLike.getFechaLike());
            playlistLikeService.save(obj);
        } else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list") 
    public List<PlaylistLikes> consultarTodo(){ 
        return playlistLikeService.findAll();
    }  
    
    @GetMapping("/list/{id}") 
    public PlaylistLikes consultaPorId(@PathVariable Integer id){ 
        return playlistLikeService.findById(id);
    }

    @GetMapping("/list/byplaylist/{idplaylist}") 
    public List<PlaylistLikes> consultaPorIdPlaylist(@PathVariable Playlists idplaylist){ 
        return (List<PlaylistLikes>) playlistLikeService.findByIdPlaylist(idplaylist);
    }     
    
    @GetMapping("/list/countlikes/{idplaylist}") 
    public Integer contarLikesPorIdPlayist(@PathVariable Integer idplaylist){ 
        return playlistLikeService.countLikesByIdPlayist(idplaylist);
    }         
}
