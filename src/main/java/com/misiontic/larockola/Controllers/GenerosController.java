/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Controllers;

import com.misiontic.larockola.Models.Generos; 
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus; 
import org.springframework.http.ResponseEntity; 
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController; 
import com.misiontic.larockola.Services.GenerosService;

/**
 *
 * @author Leonardo
 */
@RestController 
@CrossOrigin("*") 
@RequestMapping("/generos")
public class GenerosController {

    @Autowired 
    private GenerosService generoservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Generos> agregar(@RequestBody Generos genero){             
        Generos obj = generoservice.save(genero);
        return new ResponseEntity<>(obj, HttpStatus.OK);        
    }
    
    @DeleteMapping(value="delete/{id}")     
    public ResponseEntity<Generos> eliminar(@PathVariable Integer id){ 
        Generos obj = generoservice.findById(id); 
        if(obj!=null)     
            generoservice.deleteById(id);
        else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }  
    
    @PutMapping(value="/") 
    public ResponseEntity<Generos> editar(@RequestBody Generos genero){ 
        Generos obj = generoservice.findById(genero.getIdGenero()); 
        if(obj!=null) {     
            obj.setDescripcion(genero.getDescripcion());
            generoservice.save(obj); 
        } else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list") 
    public List<Generos> consultarTodo(){ 
        return generoservice.findAll(); 
    }  
    
    @GetMapping("/list/{id}") 
    public Generos consultaPorId(@PathVariable Integer id){ 
        return generoservice.findById(id);
    }
    
}
