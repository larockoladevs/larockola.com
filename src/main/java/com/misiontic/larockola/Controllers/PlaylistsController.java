/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Controllers;

import com.misiontic.larockola.Models.Playlists;
import com.misiontic.larockola.Models.Usuarios;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus; 
import org.springframework.http.ResponseEntity; 
import org.springframework.web.bind.annotation.CrossOrigin; 
import org.springframework.web.bind.annotation.DeleteMapping; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.PutMapping; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController; 
import com.misiontic.larockola.Services.PlaylistsService;
import com.misiontic.larockola.Services.UsuariosService;

/**
 *
 * @author Leonardo
 */
@RestController 
@CrossOrigin("*") 
@RequestMapping("/playlists")
public class PlaylistsController {
    
    @Autowired 
    private PlaylistsService playlistservice;

    @Autowired 
    private UsuariosService usuarioservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Playlists> agregar(@RequestBody Playlists playlist){
        Playlists obj = playlistservice.save(playlist);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="delete/{id}")     
    public ResponseEntity<Playlists> eliminar(@PathVariable Integer id){ 
        Playlists obj = playlistservice.findById(id);
        if(obj!=null)     
            playlistservice.deleteById(id);
        else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }  
    
    @PutMapping(value="/") 
    public ResponseEntity<Playlists> editar(@RequestBody Playlists playlist){ 
        Playlists obj = playlistservice.findById(playlist.getIdPlaylist()); 
        if(obj!=null) {     
            obj.setNombre(playlist.getNombre());
            obj.setIdUsuario(playlist.getIdUsuario());
            obj.setIndFavoritas(playlist.getIndFavoritas());
            obj.setIndPublica(playlist.getIndPublica());
            playlistservice.save(obj);
        } else     
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }
    
    @GetMapping("/list") 
    public List<Playlists> consultarTodo(){ 
        return playlistservice.findAll();
    }  
    
    @GetMapping("/list/{id}") 
    public Playlists consultaPorId(@PathVariable Integer id){ 
        return playlistservice.findById(id);
    }

    @GetMapping("/list/bypublica/{indpublica}") 
    public List<Playlists> consultaPorIndPublica(@PathVariable String indpublica){ 
        return playlistservice.findByIndPublica(indpublica);
    } 
    
    @GetMapping("/list/plfavorita/{email}") 
    public Playlists consultaPlaylistFavorita(@PathVariable String email ){ 
        Usuarios usuario = usuarioservice.findByEmail(email);
        if (usuario!=null) {
            return playlistservice.findFavByIdusuario(usuario.getIdUsuario());
        } else
            return null;
    }
    
}
