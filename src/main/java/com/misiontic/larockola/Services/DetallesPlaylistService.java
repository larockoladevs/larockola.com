/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.Services;

import com.misiontic.larockola.Models.Detallesplaylist;
import com.misiontic.larockola.Models.Playlists;
import java.util.List;


/**
 *
 * @author Leonardo
 */
public interface DetallesPlaylistService {
    public Detallesplaylist save(Detallesplaylist detalleplaylist);
    public void deleteById(Integer idDetPlaylist);
    public Detallesplaylist findById(Integer idDetPlaylist);
    public List<Detallesplaylist> findAll();
    public List<Detallesplaylist> findByIdPlaylist(Playlists IdPlaylist);
}
