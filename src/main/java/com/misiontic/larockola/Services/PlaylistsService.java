/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.Services;

import com.misiontic.larockola.Models.Playlists;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public interface PlaylistsService {
    public Playlists save(Playlists playlist);
    public void deleteById(Integer idPlaylist);
    public Playlists findById(Integer idPlaylist);
    public List<Playlists> findAll();
    public List<Playlists> findByIndPublica(String indPublica);
    public Playlists findFavByIdusuario(Integer idUsuario);
}
