/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.Services;

import com.misiontic.larockola.Models.Usuarios;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public interface UsuariosService {
    public Usuarios save(Usuarios usuario);
    public void deleteById(Integer idUsuario);
    public Usuarios findById(Integer idUsuario);
    public List<Usuarios> findAll();
    public Usuarios findByEmail(String email);
}
