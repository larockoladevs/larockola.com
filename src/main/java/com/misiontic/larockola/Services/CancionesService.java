/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.Services;

import com.misiontic.larockola.Models.Canciones;
import com.misiontic.larockola.Models.Generos;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public interface CancionesService {
    public Canciones save(Canciones cancion);
    public void deleteById(Integer idCancion);
    public Canciones findById(Integer idCancion);
    public List<Canciones> findAll();
    public Canciones findByNombre(String nombre);
    public List<Canciones> findByAlbum(String album);
    public List<Canciones> findByInterprete(String interprete);
    public List<Canciones> findByIdGenero(Generos genero);
}