/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.Services;

import com.misiontic.larockola.Models.PlaylistLikes;
import com.misiontic.larockola.Models.Playlists;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public interface PlaylistLikesService {
    public PlaylistLikes save(PlaylistLikes playlistLike);
    public void deleteById(Integer idPlaylistlikes);
    public PlaylistLikes findById(Integer idPlaylistlikes);
    public List<PlaylistLikes> findAll();    
    public List<PlaylistLikes> findByIdPlaylist(Playlists IdPlaylist);
    public Integer countLikesByIdPlayist(Integer id);
}
