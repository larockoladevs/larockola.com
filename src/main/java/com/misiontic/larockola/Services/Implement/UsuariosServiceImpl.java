/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Services.Implement;

import com.misiontic.larockola.Models.Usuarios; 
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;
import com.misiontic.larockola.Services.UsuariosService;
import com.misiontic.larockola.dao.UsuariosDao;
import com.misiontic.larockola.dao.UsuariosRepository;

/**
 *
 * @author Leonardo
 */
@Service
public class UsuariosServiceImpl implements UsuariosService {
    @Autowired
    private UsuariosDao usuarioDao;
    @Autowired
    private UsuariosRepository usuarioRepository;
    
    @Override
    @Transactional(readOnly=false)
    public Usuarios save(Usuarios usuario) { 
        return usuarioDao.save(usuario);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteById(Integer idUsuario) {
        usuarioDao.deleteById(idUsuario);
    }

    @Override
    @Transactional(readOnly=false)
    public Usuarios findById(Integer idUsuario) {
        usuarioDao.findById(idUsuario);
        return usuarioDao.findById(idUsuario).orElse(null);
    }
     
    @Override
    @Transactional(readOnly=false)
    public Usuarios findByEmail(String email) {
        usuarioRepository.findByEmail(email);
        return usuarioRepository.findByEmail(email);
    }
    
    @Override
    @Transactional(readOnly=false)
    public List<Usuarios> findAll() {
        return (List<Usuarios>) usuarioDao.findAll();
    }        

}
