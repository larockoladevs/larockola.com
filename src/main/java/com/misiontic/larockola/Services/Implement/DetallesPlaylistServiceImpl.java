/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Services.Implement;

import com.misiontic.larockola.Models.Detallesplaylist;
import com.misiontic.larockola.Models.Playlists;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;
import com.misiontic.larockola.Services.DetallesPlaylistService;
import com.misiontic.larockola.dao.DetallesPlaylistDao;
import com.misiontic.larockola.dao.DetallesPlaylistRepository;

/**
 *
 * @author Leonardo
 */
@Service
public class DetallesPlaylistServiceImpl implements DetallesPlaylistService {
    @Autowired
    private DetallesPlaylistDao detallePlaylistDao;

    @Autowired
    private DetallesPlaylistRepository detallePlaylistRepository;
    
    @Override
    @Transactional(readOnly=false)
    public Detallesplaylist save(Detallesplaylist playlist) { 
        return detallePlaylistDao.save(playlist);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void deleteById(Integer idDetPlaylist) {
        detallePlaylistDao.deleteById(idDetPlaylist);
    }
    
    @Override
    @Transactional(readOnly=false)
    public Detallesplaylist findById(Integer idDetPlaylist) {
        detallePlaylistDao.findById(idDetPlaylist);
        return detallePlaylistDao.findById(idDetPlaylist).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=false)
    public List<Detallesplaylist> findAll() {
        return (List<Detallesplaylist>) detallePlaylistDao.findAll();
    }

    @Override
    public List<Detallesplaylist> findByIdPlaylist(Playlists IdPlaylist) {
        detallePlaylistRepository.findByIdPlaylist(IdPlaylist);
        return (List<Detallesplaylist>) detallePlaylistRepository.findByIdPlaylist(IdPlaylist);
    }
    
}
