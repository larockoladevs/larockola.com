/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Services.Implement;

import com.misiontic.larockola.Models.Generos; 
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;
import com.misiontic.larockola.Services.GenerosService;
import com.misiontic.larockola.dao.GenerosDao;

/**
 *
 * @author Leonardo
 */
@Service
public class GenerosServiceImpl implements GenerosService {
    @Autowired
    private GenerosDao generoDao;
    
    @Override
    @Transactional(readOnly=false)
    public Generos save(Generos genero) { 
        return generoDao.save(genero);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteById(Integer idGenero) {
        generoDao.deleteById(idGenero);
    }

    @Override
    @Transactional(readOnly=false)
    public Generos findById(Integer idGenero) {
        generoDao.findById(idGenero);
        return generoDao.findById(idGenero).orElse(null);
    }

    @Override
    @Transactional(readOnly=false)
    public List<Generos> findAll() {
        return (List<Generos>) generoDao.findAll();
    }
}
