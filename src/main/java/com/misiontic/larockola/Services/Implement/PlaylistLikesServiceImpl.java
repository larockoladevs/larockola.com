/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Services.Implement;

import com.misiontic.larockola.Models.PlaylistLikes; 
import com.misiontic.larockola.Models.Playlists;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;
import com.misiontic.larockola.Services.PlaylistLikesService;
import com.misiontic.larockola.dao.PlaylistLikesDao;
import com.misiontic.larockola.dao.PlaylistLikesRepository;

/**
 *
 * @author Leonardo
 */
@Service
public class PlaylistLikesServiceImpl  implements PlaylistLikesService {
    @Autowired
    private PlaylistLikesDao playlistLikeDao;

    @Autowired
    private PlaylistLikesRepository playlistLikeRepository;
    
    @Override
    @Transactional(readOnly=false)
    public PlaylistLikes save(PlaylistLikes playlistLike) { 
        return playlistLikeDao.save(playlistLike);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void deleteById(Integer idPlaylistlikes) {
        playlistLikeDao.deleteById(idPlaylistlikes);
    }
    
    @Override
    @Transactional(readOnly=false)
    public PlaylistLikes findById(Integer idDetPlaylist) {
        playlistLikeDao.findById(idDetPlaylist);
        return playlistLikeDao.findById(idDetPlaylist).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=false)
    public List<PlaylistLikes> findAll() {
        return (List<PlaylistLikes>) playlistLikeDao.findAll();
    }    

    @Override
    public List<PlaylistLikes> findByIdPlaylist(Playlists IdPlaylist) {
        playlistLikeRepository.findByIdPlaylist(IdPlaylist);
        return (List<PlaylistLikes>) playlistLikeRepository.findByIdPlaylist(IdPlaylist);
    }

    @Override
    public Integer countLikesByIdPlayist(Integer id) {
        return playlistLikeDao.countLikesByIdPlayist(id);
    }

}
