/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Services.Implement;

import com.misiontic.larockola.Models.Playlists; 
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;
import com.misiontic.larockola.Services.PlaylistsService;
import com.misiontic.larockola.dao.PlaylistsDao;
import com.misiontic.larockola.dao.PlaylistsRepository;

/**
 *
 * @author Leonardo
 */
@Service
public class PlaylistsServiceImpl implements PlaylistsService {
    @Autowired
    private PlaylistsDao playlistDao;
    @Autowired
    private PlaylistsRepository playlistRepository;
    
    @Override
    @Transactional(readOnly=false)
    public Playlists save(Playlists playlist) { 
        return playlistDao.save(playlist);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void deleteById(Integer idPlaylist) {
        playlistDao.deleteById(idPlaylist);
    }
    
    @Override
    @Transactional(readOnly=false)
    public Playlists findById(Integer idPlaylist) {
        playlistDao.findById(idPlaylist);
        return playlistDao.findById(idPlaylist).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=false)
    public List<Playlists> findAll() {
        return (List<Playlists>) playlistDao.findAll();
    }

    @Override
    @Transactional(readOnly=false)
    public List<Playlists> findByIndPublica(String indPublica) {
        playlistRepository.findByIndPublica(indPublica);
        return (List<Playlists>) playlistRepository.findByIndPublica(indPublica);
    }

    @Override
    public Playlists findFavByIdusuario(Integer idUsuario) {
        return playlistDao.findFavByIdusuario(idUsuario);
    }
    
}
