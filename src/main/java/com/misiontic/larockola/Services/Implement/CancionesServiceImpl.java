/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Services.Implement;

import com.misiontic.larockola.Models.Canciones; 
import com.misiontic.larockola.Models.Generos;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 
import org.springframework.transaction.annotation.Transactional;
import com.misiontic.larockola.Services.CancionesService;
import com.misiontic.larockola.dao.CancionesDao;
import com.misiontic.larockola.dao.CancionesRepository;

/**
 *
 * @author Leonardo
 */
@Service
public class CancionesServiceImpl implements CancionesService {
    @Autowired
    private CancionesDao cancionDao;
    @Autowired
    private CancionesRepository cancionRepository;
    
    @Override
    @Transactional(readOnly=false)
    public Canciones save(Canciones cancion) { 
        return cancionDao.save(cancion);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteById(Integer idCancion) {
        cancionDao.deleteById(idCancion);
    }

    @Override
    @Transactional(readOnly=false)
    public Canciones findById(Integer idCancion) {
        cancionDao.findById(idCancion);
        return cancionDao.findById(idCancion).orElse(null);
    }

    @Override
    @Transactional(readOnly=false)
    public Canciones findByNombre(String nombre) {
        cancionRepository.findByNombre(nombre);
        return cancionRepository.findByNombre(nombre);
    }
    
    @Override
    @Transactional(readOnly=false)
    public List<Canciones> findAll() {
        return (List<Canciones>) cancionDao.findAll();
    }

    @Override
    @Transactional(readOnly=false)
    public List<Canciones> findByAlbum(String album) {
        return (List<Canciones>) cancionRepository.findByAlbum(album);
    }        

    @Override
    @Transactional(readOnly=false)
    public List<Canciones> findByInterprete(String interprete) {
        return (List<Canciones>) cancionRepository.findByInterprete(interprete);
    }        

    @Override
    @Transactional(readOnly=false)
    public List<Canciones> findByIdGenero(Generos genero) {
        return (List<Canciones>) cancionRepository.findByIdGenero(genero);
    }   
    
}
