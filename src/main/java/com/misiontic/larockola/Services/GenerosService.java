/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.larockola.Services;

import com.misiontic.larockola.Models.Generos;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public interface GenerosService {
    public Generos save(Generos genero);
    public void deleteById(Integer idGenero);
    public Generos findById(Integer idGenero);
    public List<Generos> findAll();
}