/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn; 
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Leonardo
 */
@Entity
@Table(name = "playlistlikes")
public class PlaylistLikes implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idplaylistlikes")
    private Integer idPlaylistlikes;

    @ManyToOne
    @JoinColumn(name = "idplaylist")
    private Playlists idPlaylist;
    
    @ManyToOne
    @JoinColumn(name = "idusuario")
    private Usuarios idUsuario;
 
    @Column(name = "fechalike")
    //@Temporal(javax.persistence.TemporalType.DATE)
    private java.sql.Timestamp fechaLike;

    public Integer getIdPlaylistlikes() {
        return idPlaylistlikes;
    }

    public void setIdPlaylistlikes(Integer idPlaylistlikes) {
        this.idPlaylistlikes = idPlaylistlikes;
    }

    public Playlists getIdPlaylist() {
        return idPlaylist;
    }

    public void setIdPlaylist(Playlists idPlaylist) {
        this.idPlaylist = idPlaylist;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    public java.sql.Timestamp getFechaLike() {
        return fechaLike;
    }

    public void setFechaLike(java.sql.Timestamp fechaLike) {
        this.fechaLike = fechaLike;
    }
        
}
