/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn; 
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Leonardo
 */
@Entity
@Table(name = "playlists")
public class Playlists implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idplaylist")
    private Integer idPlaylist;
    
    @Column(name = "nombre")
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "idusuario")
    private Usuarios idUsuario;
    
    @Column(name = "indfavoritas")
    private String indFavoritas;
    
    @Column(name = "indpublica")
    private String indPublica;

    public Integer getIdPlaylist() {
        return idPlaylist;
    }

    public void setIdPlaylist(Integer idPlaylist) {
        this.idPlaylist = idPlaylist;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Usuarios getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIndFavoritas() {
        return indFavoritas;
    }

    public void setIndFavoritas(String indFavoritas) {
        this.indFavoritas = indFavoritas;
    }

    public String getIndPublica() {
        return indPublica;
    }

    public void setIndPublica(String indPublica) {
        this.indPublica = indPublica;
    }
    
}
