/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.larockola.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn; 
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Leonardo
 */
@Entity
@Table(name = "detalleplaylist")
public class Detallesplaylist  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddetplaylist")
    private Integer idDetPlaylist;

    @ManyToOne
    @JoinColumn(name = "idplaylist")
    private Playlists idPlaylist;    
    
    @ManyToOne
    @JoinColumn(name = "idcancion")
    private Canciones idCancion;

    @Column(name = "orden")
    private Integer orden;

    public Integer getIdDetPlaylist() {
        return idDetPlaylist;
    }

    public void setIdDetPlaylist(Integer idDetPlaylist) {
        this.idDetPlaylist = idDetPlaylist;
    }

    public Playlists getIdPlaylist() {
        return idPlaylist;
    }

    public void setIdPlaylist(Playlists idPlaylist) {
        this.idPlaylist = idPlaylist;
    }

    public Canciones getIdCancion() {
        return idCancion;
    }

    public void setIdCancion(Canciones idCancion) {
        this.idCancion = idCancion;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }
       
}
