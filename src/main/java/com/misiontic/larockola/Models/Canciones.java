package com.misiontic.larockola.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn; 
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "canciones")
public class Canciones  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcancion")
    private Integer idCancion;

    @ManyToOne 
    @JoinColumn(name="idgenero") 
    private Generos idGenero;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "interprete")
    private String interprete;

    @Column(name = "anio")
    private Integer anio;
    
    @Column(name = "compositor")
    private String compositor;
    
    @Column(name = "letras")
    private java.lang.String letras;

    @Column(name = "linkstreaming")
    private String linkStreaming;

    @Column(name = "album")
    private String album;
    
    public Integer getIdCancion() {
        return idCancion;
    }

    public void setIdCancion(Integer idCancion) {
        this.idCancion = idCancion;
    }

    public Generos getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(Generos idGenero) {
        this.idGenero = idGenero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getInterprete() {
        return interprete;
    }

    public void setInterprete(String interprete) {
        this.interprete = interprete;
    }
    
    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String getCompositor() {
        return compositor;
    }

    public void setCompositor(String compositor) {
        this.compositor = compositor;
    }

    public java.lang.String getLetras() {
        return letras;
    }

    public void setLetras(java.lang.String letras) {
        this.letras = letras;
    }

    public String getLinkStreaming() {
        return linkStreaming;
    }

    public void setLinkStreaming(String linkStreaming) {
        this.linkStreaming = linkStreaming;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

}
